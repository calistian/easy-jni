#ifndef EASYJNI_ERROR_ERRORS_H_INCLUDED
#define EASYJNI_ERROR_ERRORS_H_INCLUDED

#include <exception>

namespace easyjni
{
	namespace error
	{
		class EasyJNIError : public std::exception {};

		class CantEnterThreadError : EasyJNIError
		{
		public:
			virtual const char* what() const throw()
			{
				return "Can't enter thread";
			}
		};

		class CantLeaveThreadError : EasyJNIError
		{
		public:
			virtual const char* what() const throw()
			{
				return "Can't leave thread";
			}
		};

		class NotInThreadError : EasyJNIError
		{
		public:
			virtual const char* what() const throw()
			{
				return "Not in thread";
			}
		};
	};
};

#endif