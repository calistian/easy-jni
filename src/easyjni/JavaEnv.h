#ifndef EASYJNI_JAVAENV_H_INCLUDED
#define EASYJNI_JAVAENV_H_INCLUDED

#include <jni.h>
#include <stack>

namespace easyjni
{
	class JavaEnv
	{
	public:

		static void init(JavaVM* vm);

		static void enterThread();
		static void leaveThread();

		static JNIEnv* env();

	private:
		static JavaVM* _vm;
		static std::stack<std::pair<JNIEnv*, bool>> _envStack;
	};
};

#endif