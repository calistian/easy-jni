#include "JavaEnv.h"
#include "error/errors.h"

using namespace easyjni;

JavaVM* JavaEnv::_vm = nullptr;
std::stack<std::pair<JNIEnv*, bool>> JavaEnv::_envStack;

void JavaEnv::init(JavaVM* vm)
{
	if (_vm != nullptr)
		vm = _vm;
}

void JavaEnv::enterThread()
{
	if (_vm == nullptr)
		throw error::CantEnterThreadError();
	JNIEnv* env;
	bool needDetach;
	jint getenv = _vm->GetEnv((void**)&env, JNI_VERSION_1_6);
	jint attach = 0;
	switch (getenv)
	{
	case JNI_OK:
		needDetach = false;
		break;
	case JNI_EDETACHED:
		attach = _vm->AttachCurrentThread((void**)&env, NULL);
		if (attach != JNI_OK)
			throw error::CantEnterThreadError();
		needDetach = true;
		break;
	default:
		throw error::CantEnterThreadError();
		break;
	}
	_envStack.push(std::pair<JNIEnv*, bool>(env, needDetach));
}

void JavaEnv::leaveThread()
{
	if (_vm == nullptr)
		throw error::CantLeaveThreadError();
	if (_envStack.empty())
		throw error::CantLeaveThreadError();
	if (_envStack.top().second)
		_vm->DetachCurrentThread();
	_envStack.pop();
}

JNIEnv* JavaEnv::env()
{
	if (_vm == nullptr)
		throw error::NotInThreadError();
	if (_envStack.empty())
		throw error::NotInThreadError();
	return _envStack.top().first;
}